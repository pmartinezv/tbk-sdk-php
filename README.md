# Transbank SDK Pat-pass

## Test

para probar

```bash
cd sample

php -S localhost:8888

```

ir a link [localhost:8888](http://localhost:8888)

donde se podra revisar el funcionamiento

## Pagina Inicial

 Tipo Transacción   |                                                Resumen                                                |     Ejemplos
------------------- | ----------------------------------------------------------------------------------------------------- | -----------------
Transacción PatPass | Solicitud de autorización financiera para suscribir pagos recurrentes con cargo a tarjeta de crédito. | PatPass by WebPay

## Request esperado

```json

```


## Response esperado