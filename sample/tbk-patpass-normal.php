<?php
/**
 * @author     Allware Ltda. (http://www.allware.cl)
 * @copyright  2018 Transbank S.A. (http://www.transbank.cl)
 * @date       Apr 2018
 * @license    GNU LGPL
 * @version    2.0.2
 */

require_once( '../libwebpay/patpass.php' );
require_once( 'certificates/cert-normal.php' );
session_start();

/** Configuracion parametros de la clase PatPass */
$sample_baseurl = $_SERVER['REQUEST_SCHEME'] . "://" . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'];

$configuration = new PatPassConfiguration();
$configuration->setEnvironment($certificate['environment']);
$configuration->setCommerceCode($certificate['commerce_code']);
$configuration->setPrivateKey($certificate['private_key']);
$configuration->setPublicCert($certificate['public_cert']);
$configuration->setPatPassCert($certificate['patpass_cert']);
$configuration->setCommerceMail($certificate['commerce_mail']);
$configuration->setUfFlag($certificate['uf_flag']);

/** Creacion Objeto PatPass */
$patpass = new PatPass($configuration);

$action = isset($_GET["action"]) ? $_GET["action"] : 'init';
$request = null;
$result = null;
$message = null;
$next_button = null;
$next_page = null;


switch ($action) {

    default:

        $tx_step = "Inicio de Transacci&oacute;n";

        $amount = 8880;    /** Monto de la transacción */
        $buyOrder = rand();    /** Orden de compra de la tienda */
        $sessionId = uniqid();     /** (Opcional) Código unico de session de usuario */
		$serviceId = "335456675433";             /** Identificador de Servicio asignado por TBK **/
		$cardHolderId = "11.111.111-1";          /** RUT tarjetahabiente **/
        $cardHolderName = "Juan Pedro";          /** nombres tarjetahabiente **/
        $cardHolderLastName1 = "Alarcón";        /** apellido paterno tarjetahabiente **/
        $cardHolderLastName2 = "Perez";          /** apellido materno tarjetahabiente **/
        $cardHolderMail = "example@example.com";  /** email tarjetahabiente **/
        $cellPhoneNumber = "1234567";        /** telefono tarjetahabiente **/
        $expirationDate = "2019-06-01";      /** expiración de Suscripcion YYYY-MM-DD**/

        $urlReturn = $sample_baseurl."?action=transaction";     /** URL de retorno */
	    $urlFinal  = $sample_baseurl."?action=end";    /** URL Final */

        $request = array(
            "amount"    => $amount,
            "buyOrder"  => $buyOrder,
            "sessionId" => $sessionId,
            "urlReturn" => $urlReturn,
            "urlFinal"  => $urlFinal,
            "serviceId" => $serviceId,
            "cardHolderId" => $cardHolderId,
            "cardHolderName" => $cardHolderName,
            "cardHolderLastName1" => $cardHolderLastName1,
            "cardHolderLastName2" => $cardHolderLastName2,
            "cardHolderMail" => $cardHolderMail,
            "cellPhoneNumber" => $cellPhoneNumber,
            "expirationDate" => $expirationDate
        );

        /** Iniciamos Transaccion */
        $result = $patpass->getNormalTransaction()->initTransaction(
			$request["amount"], 
			$request["buyOrder"], 
			$request["sessionId"], 
			$request["urlReturn"], 
			$request["urlFinal"],
			$request["serviceId"], 
			$request["cardHolderId"], 
			$request["cardHolderName"], 
			$request["cardHolderLastName1"], 
			$request["cardHolderLastName2"], 
			$request["cardHolderMail"], 
			$request["cellPhoneNumber"],
			$request["expirationDate"]
		);

        /** Verificamos respuesta de inicio en PatPass */
		if (!empty($result->token)) {
			/** Session Iniciada OK */
			$message = "Sesion iniciada con exito en PatPass";
			$token = $result->token;    // Token de transaccion asignado
			$next_page = $result->url;  // URL pagina de pago PatPass
			$next_button = "Ir a PatPass &raquo;";
		} else {
			$message = "PatPass no disponible";
		}
        break;

		
    case "transaction":

        $tx_step = "Obtenci&oacute;n del Resultado de la Transacci&oacute;n";

        /** Token de transaccion entregado por PatPass */
        if (empty($_POST["token_ws"])) break;
        $token = filter_input(INPUT_POST, 'token_ws');

        $request = array(
            "token" => $token
        );

        /** Rescatamos resultado y datos de la transaccion, solo permite rescatar datos 1 vez */
        $result = $patpass->getNormalTransaction()->getTransactionResult( 
			$request["token"] 
		);

        /** Verificamos resultado  de transacción */
		if (empty($result->detailOutput)) {
			$message = "Error en WS Transbank, verificar que datos y certificados sean correctos.";
			break;
		}

		if (($result->VCI == "TSY" || $result->VCI == "") && $result->detailOutput->responseCode === 0)  {
			$message = "Suscripci&oacute;n ACEPTADA por PatPass (se deben guardar datos para mostrar voucher)";
			$next_page = $result->urlRedirection;  // Url despliegue Voucher PatPass
		    $next_button = "Finalizar pago &raquo;";
			
			// Almacenamos resultado para despliegue posterior
			$_SESSION[$token] = $result;
				  
		} else {
			
			$message = "Suscripci&oacute;n RECHAZADA por PatPass (" . $result->detailOutput->responseDescription .")";
		}

        break;

    
    case "end":
        
        $tx_step = "Fin de la Transacci&oacute;n";
		
		$request = "";
		$result = $_POST;
		
        $token = filter_input(INPUT_POST, 'token_ws');

		if (!empty($token)) {
			
			if (!empty($_SESSION[$token] )) {
				// Resultado transaccion almacenados en cache
				$voucherdata = array(
					"authorizationCode" => $_SESSION[$token]->detailOutput->authorizationCode,
					"amount" => $_SESSION[$token]->detailOutput->amount,
					"buyOrder" => $_SESSION[$token]->buyOrder,
					"cardNumber" => $_SESSION[$token]->cardDetail->cardNumber,
					"sharesNumber" => $_SESSION[$token]->detailOutput->sharesNumber,
					"transactionDate" => $_SESSION[$token]->transactionDate
				);
				
				$message =  "Transacci&oacute;n Finalizada<br/>"
							."<pre>VOUCHER COMERCIO:<br/>"
							."Monto: $". $voucherdata["amount"] ."<br/>"
			                ."C&oacute;digo Autorizaci&oacute;n: ". $voucherdata["authorizationCode"] ."<br/>"
							."Orden de Compra: ". $voucherdata["buyOrder"] ."<br/>"
							."Nro Tarjeta: XXXX-". $voucherdata["cardNumber"] ."<br/>"
							."Fecha: ". $voucherdata["transactionDate"] ."<br/></pre>";			
				
				break;
			}
		}

		$message = "Transacci&oacute;n Anulada por el cliente";
		break;	
		
}
?>

<html lang="es">
<head>
  <meta charset="utf-8">
  <title>Ejemplo SDK PatPass</title>
  <style type="text/css">
  body { font-family: Tahoma, Helvetica, Arial, Verdana, sans-serif; } 
  .request { background-color:lightyellow; }
  .result { background-color:lightgrey; }
  .tbk { background-color:lightblue; }
  </style>
</head>
<body>

<h1>Ejemplo PatPass - Transacci&oacute;n Normal</h1>

<h2>Etapa: <?php  echo $tx_step; ?></h2>
<div class="request"> <h3>request</h3> <?php  var_dump($request); ?> </div>
<div class="result"> <h3>result</h3> <?php  var_dump($result); ?> </div>
<p> <?php  echo $message; ?> </p>


<?php if (!empty($next_page)) { ?>

	<form action="<?php echo $next_page; ?>" method="post">
	<input type="hidden" name="token_ws" value="<?php echo isset($token)?$token:""; ?>">
	<input type="submit" value="<?php echo $next_button; ?>">
	</form>
<?php } ?>

<br>
<a href=".">&laquo; Volver al &Iacute;ndice</a>
<br>
<br>
<body>
</html>
